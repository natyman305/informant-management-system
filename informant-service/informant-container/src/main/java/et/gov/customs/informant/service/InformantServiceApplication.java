package et.gov.customs.informant.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "et.gov.customs")
public class InformantServiceApplication {
    public static void main(String[] args){
        SpringApplication.run(InformantServiceApplication.class, args);
    }

}

package et.gov.customs.informant.service.config;

import et.gov.customs.informant.service.domain.InformantDomainService;
import et.gov.customs.informant.service.domain.InformantDomainServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

  @Bean
  public InformantDomainService informantDomainService() {
    return new InformantDomainServiceImpl();
  }
}

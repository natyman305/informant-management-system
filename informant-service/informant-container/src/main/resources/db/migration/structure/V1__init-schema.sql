
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS informant CASCADE;

CREATE TABLE informant(
      id            uuid NOT NULL,
      first_name    character varying COLLATE pg_catalog."default" NOT NULL,

      created_by    character varying COLLATE pg_catalog."default",
      created_date  TIMESTAMP not null,
      modified_by   character varying COLLATE pg_catalog."default",
      modified_date TIMESTAMP,

      CONSTRAINT informant_pkey PRIMARY KEY (id)
);
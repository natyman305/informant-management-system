package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.event.InformantCreatedEvent;
import et.gov.customs.informant.service.domain.exception.InformantDomainException;
import et.gov.customs.informant.service.domain.mapper.InformantDataMapper;
import et.gov.customs.informant.service.domain.ports.output.repository.InformantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class CreateInformantCommandHelper {

  private final InformantDataMapper informantDataMapper;
  private final InformantDomainService informantDomainService;
  private final InformantRepository informantRepository;

  public CreateInformantCommandHelper(InformantDataMapper informantDataMapper,
                                      InformantDomainService informantDomainService,
                                      InformantRepository informantRepository) {
    this.informantDataMapper = informantDataMapper;
    this.informantDomainService = informantDomainService;
    this.informantRepository = informantRepository;
  }

  @Transactional
  public InformantCreatedEvent persistInformant(CreateInformantCommand createInformantCommand) {

    Informant informant = informantDataMapper.createInformantCommandTInformant(createInformantCommand);

    InformantCreatedEvent informantCreatedEvent = informantDomainService.validateAndInitiateInformant(informant);

    saveInformant(informantCreatedEvent.getInformant());
    log.info("Informant is created with id: {}",
        informantCreatedEvent.getInformant().getId().getValue());
    return informantCreatedEvent;
  }

  private Informant saveInformant(Informant informant) {

    Informant result = informantRepository.save(informant);
    if(result == null){
      log.error("Could not save informant");
      throw new InformantDomainException("Could not save informant");
    }
    log.info("Informant is saved with id: {}"
        , result.getId().getValue());
    return result;
  }
}

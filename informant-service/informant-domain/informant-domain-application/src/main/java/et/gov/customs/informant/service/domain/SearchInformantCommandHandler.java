package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.dto.search.SearchInformantResponse;
import et.gov.customs.informant.service.domain.ports.output.repository.InformantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SearchInformantCommandHandler {

  private final InformantRepository informantRepository;

  public SearchInformantCommandHandler(InformantRepository informantRepository) {
    this.informantRepository = informantRepository;
  }

  public SearchInformantResponse searchInformant() {

    return  SearchInformantResponse.builder()
        .informants(informantRepository.findAll())
        .build();
  }

}

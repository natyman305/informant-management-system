package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantResponse;
import et.gov.customs.informant.service.domain.event.InformantCreatedEvent;
import et.gov.customs.informant.service.domain.mapper.InformantDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreateInformantCommandHandler {

  private final CreateInformantCommandHelper createInformantCommandHelper;
  private final InformantDataMapper informantDataMapper;

  public CreateInformantCommandHandler(CreateInformantCommandHelper createInformantCommandHelper,
                                       InformantDataMapper informantDataMapper) {
    this.createInformantCommandHelper = createInformantCommandHelper;
    this.informantDataMapper = informantDataMapper;
  }


  public CreateInformantResponse createInformant(CreateInformantCommand createInformantCommand) {

    InformantCreatedEvent informantCreatedEvent
        = createInformantCommandHelper.persistInformant(createInformantCommand);

    log.info("Informant is created with id: {}",
        informantCreatedEvent.getInformant().getId().getValue());
    return informantDataMapper.informantToCreateInformantResponse(
        informantCreatedEvent.getInformant(),
        "Informant created successfully"
    );
  }
}

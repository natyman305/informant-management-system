package et.gov.customs.informant.service.domain.dto.search;

import et.gov.customs.informant.service.domain.entity.Informant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchInformantResponse {
  private List<Informant> informants;
}

package et.gov.customs.informant.service.domain;

import et.gov.customs.informant.service.domain.dto.create.CreateInformantCommand;
import et.gov.customs.informant.service.domain.dto.create.CreateInformantResponse;
import et.gov.customs.informant.service.domain.dto.search.SearchInformantResponse;
import et.gov.customs.informant.service.domain.ports.input.service.InformantApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InformantApplicationServiceImpl implements InformantApplicationService {

  private final CreateInformantCommandHandler createInformantCommandHandler;
  private final SearchInformantCommandHandler searchInformantCommandHandler;


  public InformantApplicationServiceImpl(CreateInformantCommandHandler createInformantCommandHandler,
                                         SearchInformantCommandHandler searchInformantCommandHandler) {
    this.createInformantCommandHandler = createInformantCommandHandler;
    this.searchInformantCommandHandler = searchInformantCommandHandler;
  }
  @Override
  public CreateInformantResponse recordInformant(CreateInformantCommand createInformantCommand) {
    return createInformantCommandHandler.createInformant(createInformantCommand);
  }

  @Override
  public SearchInformantResponse searchInformants() {
    return searchInformantCommandHandler.searchInformant();
  }
}

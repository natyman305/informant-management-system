package et.gov.customs.informant.service.domain.event;


import et.gov.customs.informant.service.domain.entity.Informant;

import java.time.ZonedDateTime;

public class InformantCreatedEvent extends InformantEvent<Informant> {
    public InformantCreatedEvent(Informant informant, ZonedDateTime createdAt) {
        super(informant, createdAt);
    }
}

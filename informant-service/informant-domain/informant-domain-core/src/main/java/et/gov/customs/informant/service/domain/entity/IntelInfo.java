package et.gov.customs.informant.service.domain.entity;

import et.gov.customs.domain.entity.BaseEntity;
import et.gov.customs.informant.service.domain.valueobject.IntelInfoId;


public class IntelInfo extends BaseEntity<IntelInfoId> {
  private String description;

  private IntelInfo(Builder builder) {
    super.setId(builder.intelInfoId);
    description = builder.description;
  }

  public String getDescription() {
    return description;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private IntelInfoId intelInfoId;
    private String description;

    private Builder() {
    }

    public Builder intelInfoId(IntelInfoId val) {
      intelInfoId = val;
      return this;
    }

    public Builder description(String val) {
      description = val;
      return this;
    }

    public IntelInfo build() {
      return new IntelInfo(this);
    }

  }
}

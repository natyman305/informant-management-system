package et.gov.customs.informant.service.data.informant.adapter;

import et.gov.customs.informant.service.data.informant.entity.InformantEntity;
import et.gov.customs.informant.service.data.informant.mapper.InformantDataAccessMapper;
import et.gov.customs.informant.service.data.informant.repository.InformantJPARepository;
import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.ports.output.repository.InformantRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class InformantRepositoryImpl implements InformantRepository {

  private final InformantJPARepository informantJPARepository;
  private final InformantDataAccessMapper informantDataAccessMapper;

  public InformantRepositoryImpl(InformantJPARepository informantJPARepository,
                                 InformantDataAccessMapper informantDataAccessMapper) {
    this.informantJPARepository =  informantJPARepository;
    this.informantDataAccessMapper = informantDataAccessMapper;
  }

  @Override
  public Informant save(Informant informant) {
    InformantEntity entity = informantDataAccessMapper.informantToInformantEntity(informant);
    return informantDataAccessMapper.informantEntityToInformant(
        informantJPARepository.save(entity)
    );
  }

  @Override
  public List<Informant> findAll() {
    List<InformantEntity> informants = informantJPARepository.findAll();

    if(CollectionUtils.isEmpty(informants)) {
      return null;
    }

    return informants.stream()
        .map(entity -> informantDataAccessMapper.informantEntityToInformant(entity))
        .collect(Collectors.toList());
  }
}

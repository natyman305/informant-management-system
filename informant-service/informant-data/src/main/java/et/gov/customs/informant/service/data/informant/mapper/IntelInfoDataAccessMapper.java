package et.gov.customs.informant.service.data.informant.mapper;

import et.gov.customs.informant.service.data.informant.entity.InformantEntity;
import et.gov.customs.informant.service.data.informant.entity.IntelInfoEntity;
import et.gov.customs.informant.service.domain.entity.Informant;
import et.gov.customs.informant.service.domain.entity.IntelInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR
)
public interface IntelInfoDataAccessMapper {

  @Mapping(target = "id", source = "intelInfo.id.value")
  IntelInfoEntity intelInfoToIntelInfoEntity(IntelInfo intelInfo);

  @Mapping(target = "intelInfoId.value", source = "intelInfoEntity.id")
  IntelInfo intelInfoEntityToIntelInfo(IntelInfoEntity intelInfoEntity);

}

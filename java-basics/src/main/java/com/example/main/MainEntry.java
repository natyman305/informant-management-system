package com.example.main;

import com.example.oop.Car;

class MainEntry {

  public static void main(String[] args) {

    Car car = new Car("Compass", "Jeep", 2024, "Red");
    car.getMake();
    car.setMake("Test");

    System.out.println(car.toString());

    if(args == null || args.length>0) {
      for(String arg: args) {
        System.out.print(arg + " ");
      }
      System.out.println();
    } else {
      System.out.println("Hello Java World");
    }
  }
}

package et.gov.customs.notification.service.config;

import et.gov.customs.notification.service.domain.NotificationDomainService;
import et.gov.customs.notification.service.domain.NotificationDomainServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public NotificationDomainService notificationDomainService() {
        return new NotificationDomainServiceImpl();
    }
}

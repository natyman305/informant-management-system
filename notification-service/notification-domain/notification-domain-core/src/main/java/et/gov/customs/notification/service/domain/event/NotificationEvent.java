package et.gov.customs.notification.service.domain.event;

import et.gov.customs.domain.event.DomainEvent;
import et.gov.customs.notification.service.domain.entity.Notification;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class NotificationEvent implements DomainEvent<Notification> {

    private final Notification notification;
    private final ZonedDateTime createAt;

    public NotificationEvent(Notification notification) {
        this(notification, ZonedDateTime.now(ZoneId.of("UTC")));
    }

    public NotificationEvent(Notification notification, ZonedDateTime createAt) {
        this.notification = notification;
        this.createAt = createAt;
    }

    public Notification getNotification() {
        return notification;
    }

    public ZonedDateTime getCreateAt() {
        return createAt;
    }
}

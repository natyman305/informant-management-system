package et.gov.customs.notification.service.domain.event;

import et.gov.customs.notification.service.domain.entity.Notification;

import java.time.ZonedDateTime;

public class NotificationCreatedEvent extends NotificationEvent {
    public NotificationCreatedEvent(Notification notification) {
        super(notification);
    }

    public NotificationCreatedEvent(Notification notification, ZonedDateTime createAt) {
        super(notification, createAt);
    }
}

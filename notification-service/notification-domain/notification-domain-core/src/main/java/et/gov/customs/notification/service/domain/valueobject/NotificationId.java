package et.gov.customs.notification.service.domain.valueobject;

import et.gov.customs.domain.valueObject.BaseId;

import java.util.UUID;

public class NotificationId extends BaseId<UUID> {

    public NotificationId(UUID value) {
        super(value);
    }
}

package et.gov.customs.notification.service.domain.ports.input.service;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.CountUnreadNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.SearchNotificationResponse;

public interface NotificationApplicationService {

    CreateNotificationResponse createNotification(CreateNotificationCommand command);

   SearchNotificationResponse fetchNotifications();

    MarkNotificationResponse markNotification(MarkNotificationCommand markNotificationCommand);


  CountUnreadNotificationResponse countUnreadNotifications();
}

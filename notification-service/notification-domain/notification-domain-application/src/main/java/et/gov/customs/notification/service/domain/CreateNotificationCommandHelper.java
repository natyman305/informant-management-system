package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.event.NotificationCreatedEvent;
import et.gov.customs.notification.service.domain.exception.NotificationDomainException;
import et.gov.customs.notification.service.domain.mapper.NotificationDataMapper;
import et.gov.customs.notification.service.domain.ports.output.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
public class CreateNotificationCommandHelper {

    private final NotificationDataMapper notificationDataMapper;
    private final NotificationDomainService notificationDomainService;
    private final NotificationRepository notificationRepository;

    public CreateNotificationCommandHelper(NotificationDataMapper notificationDataMapper,
                                           NotificationDomainService notificationDomainService,
                                           NotificationRepository notificationRepository) {
        this.notificationDataMapper = notificationDataMapper;
        this.notificationDomainService = notificationDomainService;
        this.notificationRepository = notificationRepository;
    }

    @Transactional
    public NotificationCreatedEvent persistNotification(CreateNotificationCommand createNotificationCommand) {

        Notification notification = notificationDataMapper.createNotificationCommandToNotification(createNotificationCommand);

        NotificationCreatedEvent createdEvent = notificationDomainService.validateAndInitializeNotification(notification);

        saveNotification(createdEvent.getNotification());

        return createdEvent;
    }

    private void saveNotification(Notification notification) {
        Notification result = notificationRepository.save(notification);

        if(result == null) {
            log.error("Could not save Notification");
            throw new NotificationDomainException("Could not save Notification");
        }

        log.info("Notification is saved with id {}", result.getId().getValue());

    }
}

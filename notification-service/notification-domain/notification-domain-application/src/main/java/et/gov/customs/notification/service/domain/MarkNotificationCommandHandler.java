package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.create.CreateNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.CreateNotificationResponse;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationCommand;
import et.gov.customs.notification.service.domain.dto.create.MarkNotificationResponse;
import et.gov.customs.notification.service.domain.event.NotificationCreatedEvent;
import et.gov.customs.notification.service.domain.event.NotificationMarkedEvent;
import et.gov.customs.notification.service.domain.mapper.NotificationDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MarkNotificationCommandHandler {

    private final MarkNotificationCommandHelper markNotificationCommandHelper;
    private final NotificationDataMapper notificationDataMapper;

    public MarkNotificationCommandHandler(MarkNotificationCommandHelper markNotificationCommandHelper,
                                          NotificationDataMapper notificationDataMapper) {
        this.markNotificationCommandHelper = markNotificationCommandHelper;
        this.notificationDataMapper = notificationDataMapper;
    }
    
    

    public MarkNotificationResponse handleCreateNotification(MarkNotificationCommand command) {

        NotificationMarkedEvent notificationMarkedEvent = this.markNotificationCommandHelper.updateNotification(command);
        log.info("Notification is marked to {} for id {}",
                notificationMarkedEvent.getNotification().getStatus(),
                notificationMarkedEvent.getNotification().getId().getValue());
        return notificationDataMapper.notificationToMarkNotificationResponse(
                notificationMarkedEvent.getNotification(),
                "Successfully Notification marked");
    }
}

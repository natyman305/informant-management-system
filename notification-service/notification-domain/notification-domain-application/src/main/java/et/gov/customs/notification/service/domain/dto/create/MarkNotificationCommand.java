package et.gov.customs.notification.service.domain.dto.create;

import et.gov.customs.notification.service.domain.valueobject.NotificationStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarkNotificationCommand {
    UUID notificationId;
    NotificationStatus markTo;
}

package et.gov.customs.notification.service.domain;

import et.gov.customs.notification.service.domain.dto.search.CountUnreadNotificationResponse;
import et.gov.customs.notification.service.domain.dto.search.SearchNotificationResponse;
import et.gov.customs.notification.service.domain.entity.Notification;
import et.gov.customs.notification.service.domain.mapper.NotificationDataMapper;
import et.gov.customs.notification.service.domain.ports.output.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class SearchNotificationCommandHandler {

  private final NotificationDataMapper notificationDataMapper;
  private final NotificationRepository notificationRepository;

  public SearchNotificationCommandHandler(NotificationDataMapper notificationDataMapper,
                                          NotificationRepository notificationRepository) {
    this.notificationDataMapper = notificationDataMapper;
    this.notificationRepository = notificationRepository;
  }

  public SearchNotificationResponse searchNotifications() {
    List<Notification> notifications = notificationRepository.findAll();
    return this.notificationDataMapper.notificationToCreateNotificationResponse(notifications);    
  }

  public CountUnreadNotificationResponse countUnreadNotifications() {
    Integer noOfUnreadNotifications = notificationRepository.countUnreadNotifications();
    return CountUnreadNotificationResponse.builder().noOfUnreadNotification(noOfUnreadNotifications).build();
  }
}
